FROM node:18-alpine
COPY dist /app
WORKDIR /app
COPY package.json .
RUN npm i -g @sveltejs/kit
RUN npm i --only=prod
ENTRYPOINT node index.js