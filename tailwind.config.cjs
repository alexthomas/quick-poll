module.exports = {
    content: ['./src/**/*.{html,svelte}'],
    plugins: [
        require('@tailwindcss/forms'),
    ]
}
