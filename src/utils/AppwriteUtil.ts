import {browser} from '$app/env';
import {Appwrite} from 'appwrite';
import {account} from './Stores';

export const POLL_COLLECTION = '626ccecc3810933d20f6';
export const VOTE_COLLECTION = '626d16b371448cd1d9bd';

export const appwrite = new Appwrite();
appwrite.setEndpoint("https://appwrite.alexk8s.com/v1")
    .setProject("626db480dfcf8242f7a5");
if (browser) {
    appwrite.account.get().then(account.set).catch(error => {
        console.warn(error);
        appwrite.account.createOAuth2Session("google",window.location.toString());
    });
}
